package com.bibao.springbootvote;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.bibao.entity.CandidateEntity;
import com.bibao.entity.VoteEntity;
import com.bibao.entity.VoterEntity;
import com.bibao.repository.CandidateRepository;
import com.bibao.repository.VoteRepository;
import com.bibao.repository.VoterRepository;
import com.bibao.util.RandomSelectionUtil;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootVoteApplicationTests {
	@Value("${vote.size}")
	private int size;
	
	@Autowired
	private VoterRepository voterRepo;
	
	@Autowired
	private VoteRepository voteRepo;
	
	@Autowired
	private CandidateRepository candidateRepo;
	
	@Test
	public void contextLoads() {
		registerVoter();
		randomVote();
	}

	// Randomly register 200 voters
	private void registerVoter() {
		for (int i=0; i<size; i++) {
			VoterEntity entity = new VoterEntity();
			entity.setAge(RandomSelectionUtil.selectAge());
			entity.setState(RandomSelectionUtil.selectState().getAbbreviation());
			entity.setGender(RandomSelectionUtil.selectGender().toString());
			voterRepo.save(entity);
		}
	}
	
	// Random select a candidate for every voter
	private void randomVote() {
		List<Integer> candidateIds = candidateRepo.findAll().stream().map(CandidateEntity::getId).collect(Collectors.toList());
		List<VoterEntity> voterEntities = voterRepo.findAll();
		voterEntities.forEach(ve -> {
			VoteEntity entity = new VoteEntity();
			entity.setVoterId(ve.getId());
			entity.setCandidateId(RandomSelectionUtil.selectCandidate(candidateIds));
			voteRepo.save(entity);
		});
	}
}

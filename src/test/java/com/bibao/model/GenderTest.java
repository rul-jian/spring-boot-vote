package com.bibao.model;

import static org.junit.Assert.*;

import org.junit.Test;

public class GenderTest {

	@Test
	public void testGetGender() {
		assertEquals(Gender.MALE, Gender.getGender("Male"));
		assertEquals(Gender.FEMALE, Gender.getGender("Female"));
		assertNull(Gender.getGender("abc"));
	}

}

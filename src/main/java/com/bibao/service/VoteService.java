package com.bibao.service;

import java.util.List;

import com.bibao.model.Vote;
import com.bibao.model.VoteResult;
import com.bibao.model.VoteSummary;

public interface VoteService {
	public List<VoteSummary> fetchVoteByGender();
	public List<VoteSummary> fetchVoteByState();
	public VoteResult fetchVoteResult();
	public void saveVote(Vote vote);
}

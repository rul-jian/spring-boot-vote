package com.bibao.model;

public class Candidate {
	private int id;
	private String name;
	private String party;
	private int numOfVotes;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getParty() {
		return party;
	}
	public void setParty(String party) {
		this.party = party;
	}
	public int getNumOfVotes() {
		return numOfVotes;
	}
	public void setNumOfVotes(int numOfVotes) {
		this.numOfVotes = numOfVotes;
	}
}

package com.bibao.model;

public class VoteSummary {
	private String targetGroup;
	private int numOfRepublican;
	private int numOfDemocrat;
	
	public String getTargetGroup() {
		return targetGroup;
	}
	public void setTargetGroup(String targetGroup) {
		this.targetGroup = targetGroup;
	}
	public int getNumOfRepublican() {
		return numOfRepublican;
	}
	public void setNumOfRepublican(int numOfRepublican) {
		this.numOfRepublican = numOfRepublican;
	}
	public int getNumOfDemocrat() {
		return numOfDemocrat;
	}
	public void setNumOfDemocrat(int numOfDemocrat) {
		this.numOfDemocrat = numOfDemocrat;
	}
	
	public void increaseNumOfRepublican() {
		numOfRepublican++;
	}
	
	public void increaseNumOfDemocrat() {
		numOfDemocrat++;
	}
	
	@Override
	public String toString() {
		return "Summary[targetGroup=" + targetGroup 
				+ ", numOfRepublican=" + numOfRepublican
				+ ", numOfDemocrat=" + numOfDemocrat;
	}
}

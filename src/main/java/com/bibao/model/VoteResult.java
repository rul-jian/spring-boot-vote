package com.bibao.model;

import java.util.List;

public class VoteResult {
	private String message;
	private List<Candidate> candidates;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<Candidate> getCandidates() {
		return candidates;
	}
	public void setCandidates(List<Candidate> candidates) {
		this.candidates = candidates;
	}
}

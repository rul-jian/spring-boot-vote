package com.bibao.rest.mapper;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;

import com.bibao.model.VoteSummary;
import com.bibao.model.Voter;
import com.bibao.rest.model.VoteResponse;
import com.bibao.rest.model.VoterResponse;

public class ResponseMapper {
	public static ResponseEntity<VoterResponse> mapToVoterResponse(List<Voter> voters) {
		VoterResponse response = new VoterResponse();
		response.setVoters(voters);
		if (CollectionUtils.isEmpty(voters)) {
			response.setStatus(HttpStatus.NO_CONTENT);
			response.setMessage("No records found");
		} else {
			response.setStatus(HttpStatus.OK);
			response.setMessage("Successfully fetch data");
		}
		return new ResponseEntity<VoterResponse>(response, HttpStatus.OK);
	}
	
	public static ResponseEntity<VoterResponse> mapToVoterResponse(Voter voter, HttpStatus statusCode, String message) {
		VoterResponse response = new VoterResponse();
		response.setVoter(voter);
		response.setStatus(statusCode);
		response.setMessage(message);
		return new ResponseEntity<VoterResponse>(response, HttpStatus.OK);
	}
	
	public static ResponseEntity<VoteResponse> mapToVoteResponse(List<VoteSummary> summaries) {
		VoteResponse response = new VoteResponse();
		response.setSummaries(summaries);
		response.setStatusCode(HttpStatus.OK);
		response.setMessage("Successfully fetch data");
		return new ResponseEntity<VoteResponse>(response, HttpStatus.OK);
	}
}

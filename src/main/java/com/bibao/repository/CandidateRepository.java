package com.bibao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bibao.entity.CandidateEntity;

public interface CandidateRepository extends JpaRepository<CandidateEntity, Integer> {

}

create table voter (
	voter_id number(4) primary key,
	state char(2) not null,
	age number(3) not null,
	gender varchar2(10) not null
);

create sequence voter_seq
start with 1000
increment by 1;

create table candidate (
	candidate_id number(4) primary key,
	firstname varchar2(20) not null,
	lastname varchar2(20) not null,
	party varchar2(10) 
);

create table vote (
	vote_id number(4) primary key,
	voter_id number(4) not null,
	candidate_id number(4) not null,
	constraint fk_voter foreign key (voter_id) references voter(voter_id),
	constraint fk_candidate foreign key (candidate_id) references candidate(candidate_id),
	constraint uk_voter unique (voter_id)
);

create sequence vote_seq
start with 1
increment by 1;